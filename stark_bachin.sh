#!/bin/bash
echo "🤓  Welcome to StarkBashin' 🐺  Yo!!!"

echo 'I need Administrative Privileges to do ma thang. Do you want to continue? Enter (Y/n)'
read accessDecision
if [ $accessDecision == 'Y' ]
then
  if [ "$UID" -ne 0 ]
    then exec sudo -S "$0" "$@"
  fi
  echo "Lets get your server up and running!"
  echo "What is your first name?"
  # Ask user for their name and create user account
  read firstname
  adduser $firstname
  usermod -aG sudo $fistname

  # Set up basic firewall
  ufw app list
  ufw allow OpenSSH
  ufw enable -y
  ufw status

  # Set up ssh keys access
  mkdir -p ~/.ssh
  echo ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCSSjo4jQE9GKU6yyX8Ok2yoNNZlZsdR3wFjbGaPEkOxTyx1HA2ueNsUTy4JEMW59iLwj3fbm4CVhniC4IlqQ1GFMxUV2x5+K4AYntonpEJARweEMOsYjTZuO+gj6MFg84OIDr9F8ytEfu0PQnQ5gslbteXU0ax1wAZkoUEhXEz0y48hDeE1sj3AHIzGCoOFyTH7yqyeEY7zOldUDiTZyddnWKaURsrkoncy2rdnUuF2bbop87krAc5Fyv4/HZlGP0WtzWKYldViFJIBC1U1pGFAwDX9epQ0lUlmvkLzxF53R6syTG0ObKOan2oP7rZloX62h5w17j2n6zPIxBBA4Zt abubakarhassan59@gmail.com >> ~/.ssh/authorized_keys
  echo ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDYNYf60Tt+t2P8xbwX9j2ODh6f+wupgAbH3uTYDIBgsiVLsgQFjdWaLvBWv2JdqQfR+OfsMERvbbTT3uHBR2FdEvXZ15s6DPERhTmBprRzKMReGj6OpgO/c5iy6CGkAncNmRQ6tL9WIu7G6L7gWldvMavrhveJ8JXcvjN++c+e9C82TkQ0mYsrRX29FRfqeqyQ/yTheYiTuudy8iMwxUVWgObRBm2CDPTVfFSLP9eC5pI9WXQlG7/hReocL4BzEjHNBcGDVtLs9GQauFRLQBHKlX+dTilRBSwWP2JS9C74feq5Mk32pkP4cJIFAsHuQ6otVwftMwdTzET1GbiGu6C9 >> ~/.ssh/authorized_keys
  chmod -R go= ~/.ssh
  rsync --archive --chown=$firstname:$firstname ~/.ssh /home/$firstname

  # Set up LAMP
  apt update
  apt upgrade -y

  # Install Apache
  apt install apache2 -y
  ufw app list
  ufw app info "Apache Full"
  ufw allow in "Apache Full"
  
  # Set up mysql
  apt install mysql-server -y
  mysql_secure_installation

  # Set up PHP
  apt install php libapache2-mod-php php-mysql -y
  echo '<IfModule mod_dir.c>DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm</IfModule>' > dir.conf
  systemctl restart apache2
  touch /var/www/html/info.php
  echo '<?php phpinfo(); ?>' > /var/www/html/info.php

  # Set up git
  apt update
  apt install git -y
  git --version
  git config --global user.name "Abubakar Hassan"
  git config --global user.email "abubakarhassan59@gmail.com"

  # Set up certbot
  add-apt-repository ppa:certbot/certbot
  apt install python-certbot-apache -y
  apache2ctl configtest
  systemctl reload apache2
  ufw allow 'Apache Full'
  ufw delete allow 'Apache'
  certbot --apache --agree-tos --redirect --uir --hsts --staple-ocsp --must-staple -d ahassan-1.gcpdevops.net

  # Set up sysinfo
  mkdir ~/sysinfo
  git clone https://github.com/phpsysinfo/phpsysinfo ~/sysinfo/phpsysinfo && cp ~/sysinfo/phpsysinfo/phpsysinfo.ini.new ~/sysinfo/phpsysinfo/phpsysinfo.ini
  cp -r ~/sysinfo/phpsysinfo /var/www/html/
  echo "Done and dusted!"
else
  echo "Bye... Come back when you are ready 🙄"
fi